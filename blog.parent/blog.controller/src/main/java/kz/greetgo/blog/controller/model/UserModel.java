package kz.greetgo.blog.controller.model;

public class UserModel {


    public String id;
    public String name;
    public String surname;
//    public String email;
    public String username;
    public String password;
    public Integer user_type;
    /*
    Got 3 types of user
    ADMIN = 0
    MODERATOR = 1
    USER = 2
    */

    public UserModel(String id, String surname, String name, String password,String username, String email,Integer user_type){
        this.id = id;
        this.surname = surname;
        this.name = name;
//        this.email = email;
        this.username = username;
        this.password = password;
        this.user_type = user_type;
    }

    public UserModel(){

    }
}
