package kz.greetgo.blog.controller.controller;

import kz.greetgo.blog.controller.model.PostModel;
import kz.greetgo.blog.controller.register.PostRegister;
import kz.greetgo.blog.controller.utils.Controller;
import kz.greetgo.depinject.core.Bean;
import kz.greetgo.depinject.core.BeanGetter;
import kz.greetgo.mvc.annotations.Mapping;
import kz.greetgo.mvc.annotations.Par;
import kz.greetgo.mvc.annotations.RequestInput;
import kz.greetgo.mvc.annotations.ToJson;

import java.util.List;

@Bean
@Mapping("/post")
public class PostController implements Controller{
    public BeanGetter<PostRegister> postRegisterBeanGetter;

    @ToJson
    @Mapping("/create")
    public String createPost(@RequestInput String input){
      return postRegisterBeanGetter.get().createPost(input);
    }

    @ToJson
    @Mapping("/update")
    public String editPost(@RequestInput String input){
        return postRegisterBeanGetter.get().editPost(input);
    }
    @ToJson
    @Mapping("/delete")
    String deletePost(@Par("post_id") Integer post_id){
        return postRegisterBeanGetter.get().deletePost(post_id);
    }
    @ToJson
    @Mapping("/all")
    public List<PostModel> getAllPosts(){
        return postRegisterBeanGetter.get().getAllPosts();
    }
    @ToJson
    @Mapping("/get")
    public PostModel getPost(@RequestInput String input){
        return postRegisterBeanGetter.get().getPost(input);
    }
}
