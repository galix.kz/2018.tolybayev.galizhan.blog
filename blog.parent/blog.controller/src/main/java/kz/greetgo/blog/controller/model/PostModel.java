package kz.greetgo.blog.controller.model;

public class PostModel {

    public String id;
    public String title;
    public Integer category_id;
    public String publish_date;
    public String description;
    public Integer author_id;

    public PostModel(String id,String title,Integer category_id,String publish_date,String description,Integer author_id){
        this.id = id;
        this.title = title;
        this.category_id = category_id;
        this.publish_date = publish_date;
        this.description = description;
        this.author_id = author_id;
    }

}
