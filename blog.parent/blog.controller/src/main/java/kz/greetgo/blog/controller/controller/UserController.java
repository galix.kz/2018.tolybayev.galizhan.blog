package kz.greetgo.blog.controller.controller;

import kz.greetgo.blog.controller.model.UserModel;
import kz.greetgo.blog.controller.register.UserRegister;
import kz.greetgo.blog.controller.utils.Controller;
import kz.greetgo.depinject.core.Bean;
import kz.greetgo.depinject.core.BeanGetter;
import kz.greetgo.mvc.annotations.Mapping;
import kz.greetgo.mvc.annotations.RequestInput;
import kz.greetgo.mvc.annotations.ToJson;

@Bean
@Mapping("/user")
public class UserController implements Controller{

    public BeanGetter<UserRegister> UserRegisterBean;

    @ToJson
    @Mapping("/register")
    public String register(@RequestInput String input){
        return UserRegisterBean.get().registerUser(input);
    }

    @ToJson
    @Mapping("/update")
    public String update(@RequestInput String input){
        return UserRegisterBean.get().updateUser(input);
    }

    @ToJson
    @Mapping("/login")
    public String login(@RequestInput String input){
        return UserRegisterBean.get().login(input);
    }

    @ToJson
    @Mapping("/info")
    public UserModel info(@RequestInput String input){
        return UserRegisterBean.get().getUserInfo(input);
    }


}
