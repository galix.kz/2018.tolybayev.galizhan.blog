package kz.greetgo.blog.controller.register;

import kz.greetgo.blog.controller.model.PostModel;

import java.util.List;

public interface PostRegister {
    String createPost(String input);
    String editPost(String input);
    String deletePost(Integer post_id);
    List<PostModel> getAllPosts();
    PostModel getPost(String input);
}
