package kz.greetgo.blog.controller.controller;

import kz.greetgo.blog.controller.model.CategoryModel;
import kz.greetgo.blog.controller.register.CategoryRegister;
import kz.greetgo.blog.controller.utils.Controller;
import kz.greetgo.depinject.core.Bean;
import kz.greetgo.depinject.core.BeanGetter;
import kz.greetgo.mvc.annotations.Mapping;
import kz.greetgo.mvc.annotations.RequestInput;
import kz.greetgo.mvc.annotations.ToJson;

import java.util.List;

@Bean
@Mapping("/category")
public class CategoryController implements Controller{
    public BeanGetter<CategoryRegister> categoryBeanGetter;


    @ToJson
    @Mapping("/create")
    public String createCategory(@RequestInput String input){
        return categoryBeanGetter.get().createCategory(input);
    }
    
    @ToJson
    @Mapping("/update")
    public String updateCategory(@RequestInput String input){
        return categoryBeanGetter.get().updateCategory(input);
    }
    @ToJson
    @Mapping("/list")
    public List<CategoryModel> getCategory(){
        return categoryBeanGetter.get().getCategory();
    }

}
