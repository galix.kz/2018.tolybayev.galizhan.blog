package kz.greetgo.blog.controller.register;


import kz.greetgo.blog.controller.model.UserModel;

public interface UserRegister {
    String registerUser(String input);
    String updateUser(String input);
    UserModel getUserInfo(String input);
    String login(String input);
}
