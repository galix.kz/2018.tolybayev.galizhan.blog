package kz.greetgo.blog.controller.register;

import kz.greetgo.blog.controller.model.CategoryModel;

import java.util.List;

public interface CategoryRegister {
    String createCategory(String input);
    String updateCategory(String input);
    List<CategoryModel> getCategory();

}
