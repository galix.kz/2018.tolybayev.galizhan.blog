package kz.greetgo.blog.controller.model;

public class CategoryModel {
    public Integer id;
    public String name;

    public CategoryModel(Integer id,String name){
        this.id = id;
        this.name = name;
    }
}
