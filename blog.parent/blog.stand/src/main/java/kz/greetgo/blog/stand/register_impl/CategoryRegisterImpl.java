package kz.greetgo.blog.stand.register_impl;

import kz.greetgo.blog.controller.model.CategoryModel;
import kz.greetgo.blog.controller.register.CategoryRegister;
import kz.greetgo.depinject.core.Bean;
import org.json.JSONObject;

import java.util.List;

@Bean
public class CategoryRegisterImpl implements CategoryRegister{

    @Override
    public String createCategory(String input) {
        System.out.println("createCategory " + input);
        JSONObject obj = new JSONObject(input);
        String name = obj.getString("name");


        return "ok";
    }

    @Override
    public String updateCategory(String input) {
        return null;
    }

    @Override
    public List<CategoryModel> getCategory() {
        return null;
    }
}
