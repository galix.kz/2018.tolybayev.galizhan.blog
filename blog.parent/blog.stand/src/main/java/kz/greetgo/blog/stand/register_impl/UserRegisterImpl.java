package kz.greetgo.blog.stand.register_impl;

import kz.greetgo.blog.controller.register.UserRegister;
import kz.greetgo.depinject.core.Bean;
import org.json.JSONObject;

@Bean
public class UserRegisterImpl implements UserRegister{
    @Override
    public String registerUser(String input){
        JSONObject obj = new JSONObject(input);
        Integer uuid = obj.getInt("id");
        String name = obj.getString("name");
        String surname = obj.getString("surname");
        String username = obj.getString("username");
        String password = obj.getString("password");
        Integer user_type = obj.getInt("user_type");
        System.out.println(input);
        JSONObject r = new JSONObject().put("token","token");
        return "ok";
    }
}
