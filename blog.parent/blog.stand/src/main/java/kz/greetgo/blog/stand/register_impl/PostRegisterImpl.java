package kz.greetgo.blog.stand.register_impl;

import kz.greetgo.blog.controller.model.PostModel;
import kz.greetgo.blog.controller.register.PostRegister;
import kz.greetgo.depinject.core.Bean;
import org.json.JSONObject;

import java.util.List;

@Bean
public class PostRegisterImpl implements PostRegister{
    @Override
    public String createPost(String input) {
        System.out.println("createPost " + input);
        JSONObject obj = new JSONObject(input);
        String title = obj.getString("title");
        Integer category_id = obj.getInt("category_id");
        String description = obj.getString("description");

        return "pl";
    }

    @Override
    public String editPost(String input) {
        return "ok";
    }

    @Override
    public String deletePost(Integer post_id) {
        return "ok";
    }

    @Override
    public List<PostModel> getAllPosts() {
        return null;
    }

    @Override
    public PostModel getPost(String input) {
        return null;
    }
}
