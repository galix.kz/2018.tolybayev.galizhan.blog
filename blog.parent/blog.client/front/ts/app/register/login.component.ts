import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpService} from "../../provider/HttpService";

import "rxjs/add/operator/toPromise";
import {toPromise} from "rxjs/operator/toPromise";
const node_modules = "../../../../node_modules/";
@Component({
    selector: 'login_app',
    template: require('./login.component.html'),
    styles: [require('./login.component.css'),]
})

export class LoginComponent implements OnInit {
    public text:string="sdasd";
    constructor(private router:Router, private httpService:HttpService) { }

    ngOnInit() {

    }
    registerUser(e) {
        e.preventDefault();
        console.log(e);

        let username = e.target.elements[0].value;
        let name = e.target.elements[1].value;
        let surname = e.target.elements[2].value;
        let password = e.target.elements[3].value;

        console.log(username,password);
        let data = JSON.stringify({username:username,name:name,surname:surname,user_type:0, password:password});
        console.log(data)
        this.httpService.postJson("/user/register",
            (data)
        ).toPromise().then(
            result =>{
                console.log(result);

            },
            error =>{
                    this.text="Something is wrong";
            }
        )

    }
    loginUser(e) {
        e.preventDefault();
        console.log(e);
        var username = e.target.elements[0].value;
        var password = e.target.elements[1].value;
        var data = JSON.stringify({username:username, password:password});
        this.httpService.postJson("/user/login",
            (data)
        ).toPromise().then(
            result =>{
                console.log(result);

            },
            error =>{
                this.text="Something is wrong";
            }
        )

    }
}