import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import {HttpService} from "../../provider/HttpService";

import "rxjs/add/operator/toPromise";
import {toPromise} from "rxjs/operator/toPromise";
const node_modules = "../../../../node_modules/";
@Component({
    selector: 'post_edit_app',
    template: require('./post-edit.component.html'),
    styles: [require('./post-edit.component.css')]
})

export class PostEditComponent implements OnInit {
    public text:string="sdasd";
    constructor(private router:Router, private httpService:HttpService) { }

    ngOnInit() {

    }
    createOrUpdatePost(e) {
        e.preventDefault();
        console.log(e);

        let title = e.target.elements[0].value;
        let category_id = e.target.elements[1].value;
        let description = e.target.elements[2].value;

        let data = JSON.stringify({title:title,category_id:category_id,description:description});
        console.log(data)
        this.httpService.postJson("/post/create",
            (data)
        ).toPromise().then(
            result =>{
                console.log(result);
                this.text=result.json().text;
                // this.router.navigate([this.text]);
            },
            error =>{
                this.text="Something is wrong";
            }
        )

    }

}