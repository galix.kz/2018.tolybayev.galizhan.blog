import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {LoginComponent} from "./register/login.component";
import {MainComponent} from "./main/main.component";
import {PostComponent} from "./post/post.component";
import {PostDetailViewComponent} from "./post-detail-view/post-detail.component";
import {PostEditComponent} from "./post-edit/post-edit.component";
import {BrowserModule} from "@angular/platform-browser";
import {HttpService} from "../provider/HttpService";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import { RouterModule, Routes,UrlHandlingStrategy } from "@angular/router";
import {APP_BASE_HREF} from "@angular/common";

const appRoutes:Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: MainComponent
    },

]
@NgModule({
  imports:[
    BrowserModule, HttpModule,RouterModule.forRoot([{
              path: 'login-or-register',
              component: LoginComponent
          },
          {
              path: '',
              component: MainComponent
          },
          {
              path:'post-detail',
              component: PostDetailViewComponent
          },
          {
              path:'post-edit',
              component: PostEditComponent
          }
         ],{useHash:true})
  ],
  //   imports:[
  //       BrowserModule,HttpModule
  //   ],
  declarations:[AppComponent,LoginComponent,MainComponent,PostComponent,PostDetailViewComponent,PostEditComponent],
  bootstrap:[AppComponent],
  entryComponents:[AppComponent],
    providers:[HttpService, {provide: APP_BASE_HREF, useValue: ''}],
    // providers:[HttpService],
})
export class AppModule {

}