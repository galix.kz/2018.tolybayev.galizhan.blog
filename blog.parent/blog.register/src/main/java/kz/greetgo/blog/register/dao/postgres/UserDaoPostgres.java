package kz.greetgo.blog.register.dao.postgres;
import kz.greetgo.depinject.core.Bean;
import kz.greetgo.blog.register.dao.UserDao;

@Bean
public interface UserDaoPostgres extends UserDao {
}
