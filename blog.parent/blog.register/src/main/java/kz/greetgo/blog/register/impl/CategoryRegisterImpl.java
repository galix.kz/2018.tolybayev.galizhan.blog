package kz.greetgo.blog.register.impl;

import kz.greetgo.blog.controller.model.CategoryModel;
import kz.greetgo.blog.controller.register.CategoryRegister;
import kz.greetgo.depinject.core.Bean;

import java.util.List;

@Bean
public class CategoryRegisterImpl implements CategoryRegister{
    @Override
    public String createCategory(String input) {
        return null;
    }

    @Override
    public String updateCategory(String input) {
        return null;
    }

    @Override
    public List<CategoryModel> getCategory() {
        return null;
    }
}
