package kz.greetgo.blog.register.dao;


import kz.greetgo.blog.controller.model.UserModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserDao {

    @Insert("insert into user_table(name, surname, password,username,user_type)" +
            " values (#{name},#{surname},#{password},#{username}, #{user_type})")
    void registerUser(@Param("name") String name,
                      @Param("surname") String surname,
//                      @Param("email") String email,
                      @Param("password") String password,
                      @Param("username") String username,
                      @Param("user_type") Integer user_type
                      );

    @Update("update user_table " +
            "set name = #{name}, surname = #{surname}, password = #{password},user_type = #{user_type} " +
            "where id = #{user_id}")
    void updateUser(@Param("name") String name,
                      @Param("surname") String surname,
//                      @Param("email") String email,
                      @Param("password") String password,
                      @Param("user_type") Integer user_type,
                    @Param("user_id") Integer user_id
    );

    @Select("select id from user_table where username=#{username} and password=#{password}")
    Integer getUserIdSelect(@Param("username") String username,
                           @Param("password") String password);

    @Select("select user_id from token_table where token=#{token}")
    Integer getUserIdByToken(@Param("token") String token);

    @Select("select token from token_table where user_id=#{user_id}")
    String getUserToken(@Param("user_id") Integer user_id);

    @Insert("insert into token_table (token, user_id) values(#{token}, #{user_id})")
    void insertToken(@Param("user_id") Integer user_id,
                     @Param("token") String token);

    @Update("update token_table set token = #{token} where user_id = #{user_id}")
    void updateToken(@Param("user_id") Integer user_id,
                     @Param("token") String token);
}