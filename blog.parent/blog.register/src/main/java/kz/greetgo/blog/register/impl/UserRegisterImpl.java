package kz.greetgo.blog.register.impl;

import kz.greetgo.blog.controller.model.UserModel;
import kz.greetgo.depinject.core.Bean;
import kz.greetgo.depinject.core.BeanGetter;
import kz.greetgo.blog.controller.register.UserRegister;
import kz.greetgo.blog.register.dao.UserDao;
import org.json.JSONObject;

import java.util.Base64;

@Bean
public class UserRegisterImpl implements UserRegister {

    public BeanGetter<UserDao> userDaoBeanGetter;

    @Override
    public String registerUser(String input) {
        String render = "";
        JSONObject obj = new JSONObject(input);
        String name = obj.getString("name");
        String surname = obj.getString("surname");
        String username = obj.getString("username");
        String password = obj.getString("password");
        Integer user_type = obj.getInt("user_type");
        userDaoBeanGetter.get().registerUser(name, surname, password, username,user_type);
        Integer user_id = userDaoBeanGetter.get().getUserIdSelect(username,password);
        String toEncode = username+" "+password;
        String token = Base64.getEncoder().encodeToString(toEncode.getBytes());
        userDaoBeanGetter.get().insertToken(user_id,token);
        return token;
    }

    @Override
    public String updateUser(String input) {
        JSONObject obj = new JSONObject(input);
        String name = obj.getString("name");
        String surname = obj.getString("surname");
        String password = obj.getString("password");
        Integer user_type = obj.getInt("user_type");
        String token = obj.getString("token");
        Integer user_id = userDaoBeanGetter.get().getUserIdByToken(token);
        userDaoBeanGetter.get().updateUser(name,surname,password,user_type,user_id);
        return "ok";
    }

    @Override
    public UserModel getUserInfo(String input) {
        return null;
    }

    @Override
    public String login(String input) {
        JSONObject obj = new JSONObject(input);
        String username = obj.getString("username");
        String password = obj.getString("password");
        Integer user_id = userDaoBeanGetter.get().getUserIdSelect(username,password);
        return userDaoBeanGetter.get().getUserToken(user_id);
    }

}