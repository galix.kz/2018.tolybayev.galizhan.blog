package kz.greetgo.blog.register.impl;

import kz.greetgo.blog.controller.register.UserRegister;
import kz.greetgo.blog.register.dao.TestDao;
import kz.greetgo.blog.register.dao.UserDao;
import kz.greetgo.blog.register.test.util.BeanContainerForTest;
import kz.greetgo.blog.register.util.JdbcBlog;
import kz.greetgo.db.ConnectionCallback;
import kz.greetgo.depinject.core.BeanGetter;
import kz.greetgo.depinject.testng.AbstractDepinjectTestNg;
import kz.greetgo.depinject.testng.ContainerConfig;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Base64;

import kz.greetgo.util.RND;
import static org.testng.Assert.*;

@ContainerConfig(BeanContainerForTest.class)
public class UserRegisterImplTest extends AbstractDepinjectTestNg {
    public BeanGetter<UserDao> userTestDaoBeanGetter;
    public BeanGetter<JdbcBlog> jdbcBlog;
    public BeanGetter<TestDao> testDao;

    @Test
    public void testFotUserRegister() {
        String userid= "1";
        String name = "a123123sd";
        String surname = "qwe";
        String username = "3";
        String email = "qweasdxc";
        String password = "qwesad";
        String user_type = "2";
//        userTestDaoBeanGetter.get().registerUser( name, surname,password,username,0);
//        Integer user_id = userTestDaoBeanGetter.get().getUserIdSelect(username,password);
//        String toEncode = username+" "+password;
//        String token = Base64.getEncoder().encodeToString(toEncode.getBytes());
//        userTestDaoBeanGetter.get().insertToken(user_id,token);
//        System.out.println(token);

        userTestDaoBeanGetter.get().updateUser(name,surname,password,0,1);
    }
}